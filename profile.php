<?php
 session_start();

 require("includes/init.php");
 include('filters/auth_filter.php');


 if(!empty($_GET['id'])){
 //Recuperer les infos sur l'user en bdd en utilisant son id
     $user = find_user_by_id($_GET['id']);

     if(!$user){
     redirection('index.php');
     } else {
     $query = $db->prepare("SELECT U.id id_user, U.pseudo, U.email, U.avatar,
                         M.id m_id, M.content,M.like_count, M.created_at
                         FROM users U, microposts M, friends_relationships F
                         WHERE M.user_id = U.id

                         AND

                         CASE
                         WHEN F.id1_user = :id_user
                         THEN F.id2_user = M.user_id

                         WHEN F.id2_user = :id_user
                         THEN F.id1_user = M.user_id
                         END

                         AND F.status > 0
                         ORDER BY M.id DESC");

     $query->execute([
       'id_user' => $_GET['id']
     ]);

     $microposts = $query->fetchAll(PDO::FETCH_OBJ);
     }

 } else {

       redirection('profile.php?id='.get_session('id_user'));
 }


  // le formulaire a ete soumis
   if (isset($_POST['update'])) {

   	    $erros =[];

     	 // si tous les champs ont ete remplies
     	if (no_empty(['name','city','country','sexe','bio']) )
        {
           extract($_POST);

        $query= $db->prepare("UPDATE users SET name =:name,city =:city,
        	country=:country,sexe =:sexe,twitter =:twitter,github =:github,
        	avaible_for_hiring =:avaible_for_hiring, bio =:bio WHERE id =:id");

     		$query->execute(
              [
                'name'=>$name,
                'city'=>$city,
                'country'=>$country,
                'sexe'=>$sexe,
                'twitter'=>$twitter,
                'github'=>$github,
                'avaible_for_hiring'=>!empty($avaible_for_hiring) ? '1' : '0',
                'bio'=>$bio,
                'id'=>get_session('id_user')
              ]);

               set_flash("Felicitation, votre profile a ete mis a jour!");
               redirection('profile.php?id='.get_session('id_user'));
        }else{
            // On garde les donnees saisies dans champs inputs
            garder_infos_saisis();
            $erros[] = "Veuillez remplir tous les champs marquereyues d'un (*)" ;
        }

      
    } else{
     // permettant de nettoyer les donnees garder en session
     supprimer_les_donnees();
   }





 require("views/profile.view.php"); 


 ?>