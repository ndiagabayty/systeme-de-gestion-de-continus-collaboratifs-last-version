<?php 

 session_start();
 require("includes/init.php");
 //require("filters/auth_filter.php");


	$nbre_total_users = friends_count(get_session('id_user'));

	$nbre_users_par_page = 12;

	$nbre_pages_max_gauche_et_droite = 4;

	$last_page = ceil($nbre_total_users / $nbre_users_par_page);

	if(isset($_GET['page']) && is_numeric($_GET['page'])){
		$page_num = $_GET['page'];
	} else {
		$page_num = 1;
	}

	if($page_num < 1){
		$page_num = 1;
	} else if($page_num > $last_page) {
		$page_num = $last_page;
	}

	$limit = 'LIMIT '.($page_num - 1) * $nbre_users_par_page. ',' . $nbre_users_par_page;
    
    //if (current_user_is_freind_with(get_session('id_user'))) {
    	
    	$query = $db->query("SELECT U.id , U.pseudo, U.email, U.avatar, F.id1_user, F.id2_user
                              
                             FROM users U, friends_relationships F
                             WHERE (F.id1_user =U.id AND F.id2_user= U.id)
                             
                             ORDER BY U.pseudo $limit

                         ");
    
    $users = $query->fetchALL(PDO::FETCH_OBJ);
    
	$pagination = '<nav ><ul class="pagination">';

	if($last_page != 1){
		if($page_num > 1){
			$previous = $page_num - 1;
			$pagination .= '<li><a href="liste_amis.php?page='.$previous.'">Précédent</a></li> ';

			for($i = $page_num - $nbre_pages_max_gauche_et_droite; $i < $page_num; $i++){
				if($i > 0){
					$pagination .= '<li><a href="liste_amis.php?page='.$i.'">'.$i.'</a></li>';
				}
			}
		}

		$pagination .= '<li class="active"><a href="#">'.$page_num.'</a></li>';

		for($i = $page_num+1; $i <= $last_page; $i++){
			$pagination .= '<li><a href="liste_amis.php?page='.$i.'">'.$i.'</a></li> ';
			
			if($i >= $page_num + $nbre_pages_max_gauche_et_droite){
				break;
			}
		}

		if($page_num != $last_page){
			$next = $page_num + 1;
			$pagination .= '<li><a href="liste_amis.php?page='.$next.'">Suivant</a></li> ';
		}
	}

		
$pagination .='</ul></nav>';

 require("views/liste_amis.view.php"); 
//}
//}

 ?>