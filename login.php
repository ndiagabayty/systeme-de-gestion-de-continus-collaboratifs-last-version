<?php $title = "Inscription"; ?>
<?php

   // Pour pouvoir utliser $_SESSION  77 109 05 93
   session_start();

   require("includes/init.php");
   require("filters/guest_filter.php");
  
   
  // le formulaire a ete soumis
   if (isset($_POST['login'])) {

     	 // si tous les champs ont ete remplies
     	if (no_empty(['identifiant','password']) )
      {
        extract($_POST);

        $query= $db->prepare("SELECT id,pseudo,avatar,password AS hashed_password,email  FROM users

          WHERE (pseudo = :identifiant OR email = :identifiant)
          AND  active = '1'  ");
         

     		$query->execute(
              [
                'identifiant'=>$identifiant,
                
              ]);
          
        // On compte le nombre de l'utilisateur trouves
        $user = $query->fetch(PDO::FETCH_OBJ) ;


        if ($user && bcrypt_verify_password($password,$user->hashed_password)) {


            // $user = $query->fetch(PDO::FETCH_OBJ);
  
             $_SESSION['id_user'] = $user->id;
             $_SESSION['pseudo'] = $user->pseudo;
             $_SESSION['email'] = $user->email;
             $_SESSION['avatar'] = $user->avatar;
            
            // On stocke les infos dans la table connected
             $query = $db->prepare("INSERT INTO connected(id_user,pseudo,email,avatar)
                                    VALUES (:id_user,:pseudo,:email,:avatar)
                                  ");
             $query->execute(
              [
                'id_user'=>$_SESSION['id_user'],
                'pseudo'=>$_SESSION['pseudo'],
                'email'=>$_SESSION['email'],
                'avatar'=>$_SESSION['avatar']
              ]);


             // On verifie est ce que l'utilisateur a coche la case garder ma session active

             if (isset($_POST['remember_me']) && $_POST['remember_me']=='on') {
                 // Creation des cookies
                remember_me($user->id);
             }
             set_flash("Bienvenu dans votre page de profil!");
             redirection_entension('profile.php?id='.$user->id);
        }
        else{
          set_flash("Combinaison identifiant/mot de passe incorrect!",'danger');
          // On garde les donnees saisies dans champs inputs
          garder_infos_saisis();
        }

      }
   	
    } else{
     // permettant de nettoyer les donnees garder en session
     supprimer_les_donnees();
   }

?>




<?php

    require("views/login.view.php");
?>
