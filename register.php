<?php $title = "Inscription"; ?>
<?php

   // Pour pouvoir utliser $_SESSION  77 109 05 93
   session_start();
   require("includes/init.php");
   require("filters/guest_filter.php");
   
  // le formulaire a ete soumis
   if (isset($_POST['register'])) {

   	 // si tous les champs ont ete remplies
   	if (no_empty(['name','pseudo','email','password','password_confirm']) )
    {
   		$errors =[]; // tableau contenant l4ensemble des erreurs

   		extract($_POST); // permettant d'acceder a tous les elememts

   		if (mb_strlen($pseudo) < 3 ){
   			 $errors[] ="Pseudo trop court (Minimum 3 caracteres)!";
   		}
   		if (!filter_var($email,FILTER_VALIDATE_EMAIL)) {
   			$errors[]="Adresse email invalide!";
   		}
   		if (mb_strlen($password) < 6 ){
   			 $errors[] ="Mot de passe trop court (Minimum 6 caracteres)!";
   		} else {
   			if ($password!=$password_confirm) {
   				$errors[]="Vos deux mots de passes ne concordent pas!";
   				
   			}
   		}

   		if (is_already_in_use('pseudo',$pseudo,'users')) {
   			$errors[]="Pseudo deja utilise!";
   		}
        if (is_already_in_use('email',$email,'users')) {
   			$errors[]="Adresse-Email deja utilise!";
   		}
        

        if (count($errors)==0) {

        	// Envoi d'un mail d'actvation
        	$to=$email;
        	$subject=WEBSITE_NAME.'-ACTIVATION DE COMPTE';
          $password = bcrypt_hash_password($password);
          $token=sha1($pseudo.$email.$password);
            
            // garder les infos en memoire tempon
            ob_start();
            require("template/emails/activation.tmpl.php");
            $content= ob_get_clean();

            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            //ini_set("SMTP","smtp.gmail.com"); 
            //ini_set("smtp_port","25");

            mail($to,$subject,$content,$headers);
            

            set_flash("Mail d'activation envoye!",'success');
            // On enregistre les infos de l'utilisateurbdans la base de donnee
            $query= $db->prepare('INSERT INTO users(name,pseudo,email,password) 
                                  VALUES(:name,:pseudo,:email,:password) ');
             $query->execute(
              [
                'name'=>$name,
                'pseudo'=>$pseudo,
                'email'=>$email,
                'password'=>$password
              ]);
              
              $query = $db->prepare("UPDATE users SET active ='1'  WHERE pseudo=?");
              $query->execute([$pseudo]);

                // On recupere le pseudo et l'id
              $query = $db->prepare('SELECT id,email,password FROM users WHERE pseudo=?');
              $query->execute([$pseudo]);

              // on recupere les infos sous forme d'objet

              $data = $query->fetch(PDO::FETCH_OBJ);

              $query = $db->prepare("INSERT INTO friends_relationships(id1_user,id2_user,status)
                              VALUES (?,?,?)");
              $query->execute([$data->id,$data->id,'2']);



            redirection('login.php');

        }else{
          garder_infos_saisis();
        }

   	} else {
   		 $errors ="Veuillez remplir tous les champs!";
       // garder les infos en session des au'on trouve une erreur
       garder_infos_saisis();
   	}

   	
   } else{
     // permettant de nettoyer les donnees garder en session
     supprimer_les_donnees();
   }

?>




<?php

    require("views/register.views.php");
?>
