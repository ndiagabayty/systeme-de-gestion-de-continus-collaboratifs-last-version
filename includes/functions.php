<?php

 // Le nombre de like d'un micropost donnee
if (!function_exists('get_like_count')) {
  
  function get_like_count($micropost_id){
      
    global $db;
    $query = $db->prepare("SELECT like_count FROM microposts WHERE id = :id");
    $query->execute(['id'=>$micropost_id]);

    $data = $query->fetch(PDO::FETCH_OBJ);

    return intval($data->like_count);
        
  }
 }

 // display liker 
if (!function_exists('get_likers')) {
  
  function get_likers($micropost_id){
      
      global $db;
      $query = $db->prepare("SELECT users.id, users.pseudo 
                             FROM users
                             LEFT JOIN micropost_like 
                             ON users.id=micropost_like.id_user
                             WHERE micropost_like.id_micropost = :id_micropost
                             ORDER BY micropost_like.id DESC
                             LIMIT 3
                             ");

      $query->execute(
        [
        'id_micropost'=>$micropost_id
        ]);
   

    return $query->fetchAll(PDO::FETCH_OBJ);
        
  }
 }




 // display liker 
if (!function_exists('get_likers_text')) {
  
  function get_likers_text($micropost_id){
      
    $like_count = get_like_count($micropost_id);
    $likers = get_likers($micropost_id);
    $remaining_liker= $like_count-3;
    $itself_like = user_has_already_like_the_micropost($micropost_id);
    $output= '';
    if ($like_count>0) {
        foreach ($likers as $liker) {
            if (get_session('id_user') !== $liker->id) {
                  $output.='<a href= "profile.php?id='.$liker->id.'">'.$liker->pseudo.'</a>,' ;
            }
           
        }
        $output = $itself_like ? 'Vous, '.$output : $output;


        if (($like_count==2 || $like_count==3) && $output!="") {
           $output = trim($output, ', ');
           $arr = explode(",", $output);
           $lastItem = array_pop($arr);
           $output = implode(', ', $arr);
           $output.=' et '.$lastItem;
         } 
         $output = trim($output, ', ');
        switch ($like_count){
          case 1:
            $output .=$itself_like ? ' aimez cela.' : ' aime cela';
            break;

          case 2:
          case 3:
            $output .=$itself_like ? ' aimez cela.' : ' aiment cela.';
            break;
          case 4:
            $output .=$itself_like ? ' et 1 autre personne aimez cela.' 
                                   : ' et 1 autre personne aiment cela.';
            break;      
          
          default:
            $output .=$itself_like ? ' et '.$remaining_liker.' autres personnes aimez cela.'
                                   : ' et '.$remaining_liker.' autres personnes aiment cela.';
            break;
        }
      
    }
    
    return $output;
        
  }
 }



// Aimer un micropost
if(!function_exists('like_micropost')){
    function like_micropost($micropost_id){
      global $db;
      $query = $db->prepare("INSERT INTO micropost_like(id_user,id_micropost) 
                           VALUES (:id_user,  :id_micropost) 
                         ");
        $query->execute(
        [
           'id_user'=> get_session('id_user'),
           'id_micropost'=> $micropost_id
        ] );

        $query = $db->prepare("UPDATE microposts SET like_count = like_count+1 
                             WHERE id=:id_micropost ");
        
                                              
      $query->execute(
        [
          'id_micropost'=> $micropost_id
        ]);
        
    }
}

// Ne aimer un micropost
if(!function_exists('unlike_micropost')){
    function unlike_micropost($micropost_id){
      global $db;
     $query = $db->prepare("DELETE FROM micropost_like 
                             WHERE id_user=:id_user AND id_micropost =:id_micropost 
                         ");
      $query->execute(
      [
         'id_user'=> get_session('id_user'),
         'id_micropost'=> $micropost_id
      ] );

      $query = $db->prepare("UPDATE microposts SET like_count = like_count-1 
                           WHERE id=:id_micropost ");
     
                                            
    $query->execute(
      [
        'id_micropost'=> $micropost_id
      ]);
    
        
    }
}

// Permet de creer un micropost pour l'utilisateur connecte
if(!function_exists('create_micropost_for_the_current_user')){
    function create_micropost_for_the_current_user($content){
      global $db;
      $query = $db->prepare("INSERT INTO microposts(content,user_id,created_at) 
                                   VALUES(:content,:user_id,NOW())");
             $query->execute(
               [
                  ':content' => $content, 
                  ':user_id' => get_session('id_user')
               ]
               );
              set_flash("Votre statut a ete mis a jour!");
        
    }
}
   

if(!function_exists('replace_links')){
    function replace_links($texte){
        return preg_replace(array('/(?i)\b((?:https?:\/\/|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))\)|[^\s`!()\[\]{};:\'".,<>?«»“”‘’]))/', '/(^|[^a-z0-9_])@([a-z0-9_]+)/i', '/(^|[^a-z0-9_])#([a-z0-9_]+)/i'), array('<a href="$1" target="_blank">$1</a>', '$1<a href="">@$2</a>', '$1<a href="index.php?hashtag=$2">#$2</a>'), $texte);
    }
}


//Check if a current user has already like the micropost
if (!function_exists('user_has_already_like_the_micropost')) {
  
  function user_has_already_like_the_micropost($micropost_id){
      global $db;
      $query = $db->prepare("SELECT id FROM micropost_like 
                         WHERE id_user=:id_user AND id_micropost =:id_micropost ");                    
      $query->execute(
        [
           'id_user'=>get_session('id_user'),
               'id_micropost'=>$micropost_id
        ]);
        
      return (bool) $query->rowCount();
  }
 }
// Check if a friend request already has been sent
if (!function_exists('if_a_friend_request_already_has_been_sent')) {
  
  function if_a_friend_request_already_has_been_sent($id1,$id2){
      global $db;
      $query = $db->prepare("SELECT status
                             FROM friends_relationships
                             WHERE (id1_user = :id1_user AND id2_user= :id2_user)
                             OR (id1_user = :id2_user AND id2_user= :id1_user) ");
      $query->execute(
        [
           'id1_user' => $id1,
           'id2_user' => $id2
        ]);
      $count = $query->rowCount();
      $query->closeCursor();
      return (bool)$count;
  }
 }

 // Verifie si deux utilisateurs sont amies
if (!function_exists('current_user_is_freind_with')) {
  
  function current_user_is_freind_with($id_second_user){
      global $db;
      $query = $db->prepare("SELECT status
                             FROM friends_relationships
                             WHERE (id1_user = :id1_user AND id2_user= :id2_user)
                             OR (id1_user = :id2_user AND id2_user= :id1_user) ");
      $query->execute(
        [
           'id1_user' => get_session('id_user'),
           'id2_user' => $id_second_user
        ]);
      $count = $query->rowCount();
      $query->closeCursor();
      return (bool)$count;
  }
 }
// compter le nombre d'amies
if (!function_exists('friends_count')) {
  
  function friends_count($id){
      global $db;
      $query = $db->prepare("SELECT status FROM friends_relationships
                             WHERE id1_user = :user_connected OR  id2_user = :user_connected
                             AND status ='1' ");
      $query->execute(
        [
           'user_connected' =>$id
        ]);
      $count = $query->rowCount();
      $query->closeCursor();
      return $count;
  }
 }
// Verifier si un utilisateur a envoye ou recevoir une demande d'amitie
if (!function_exists('relation_link_to_display')) {
  
  function relation_link_to_display($id){
      global $db;

      $query = $db->prepare("SELECT id1_user,id2_user, status
                             FROM friends_relationships
                             WHERE (id1_user = :id1_user AND id2_user= :id2_user)
                             OR (id1_user = :id2_user AND id2_user= :id1_user)
                           ");
      $query->execute(
        [
            'id1_user'  =>get_session('id_user'),
            'id2_user' =>$id
        ]
        );
      $data=$query->fetch();

      if ($data['id1_user']==$id && $data['status']=='0') {
        # Lien pour accepter ou refuser la demande
        return "accept_rejet_relation_link";

      }elseif ($data['id1_user']==get_session('id_user') && $data['status']=='0') {
        # Msg pour dire que demande a deja ete  envoye 
        // Lien pour annuler
        return "cancel_relation_link";
      }elseif(($data['id1_user']==get_session('id_user') OR $data['id1_user']==$id) 
      AND $data['status']=='1')
        {
        # Lien pour supprimer la relation d'amitie
          return "delete_relation_link";
      }
      else{

        // ajouter la personne comme ami
        return "add_relation_link";
      }
  }
 }

// recuperer la session selon la cle
if (!function_exists('get_session')) {
 	
 	function get_session($key){
 			if ($key) {
 				return !empty($_SESSION[$key])
 				? echappe($_SESSION[$key])
 				:null ;
 			}
 	}
 }
// cell_count
 // Retourne le nombre d'enregistrement trouve selon une certaine condition
 if (!function_exists('cell_count')) {
  
  function cell_count($table,$field_name,$field_value){
      global $db;
      $query = $db->prepare("SELECT * FROM $table WHERE $field_name = ?");
      $query->execute([$field_value]);
      return $query->rowCount();
  }
 }

 // Remember me
if (!function_exists('remember_me')) {
  
  function remember_me($id_user){
    global $db;
    // Generer le token de maniere aleatoire
    $token = openssl_random_pseudo_bytes(24);
    //Generer le token de maniere aleatoire
    // et s'assurer que ce dernier est unique

    do{
        $selector = openssl_random_pseudo_bytes(9);
    }while (cell_count('auth_tokens', 'selector',$selector) >0 ) ;
    
    // Sauver ces infos (id_user, selector,expires(14 jours), token(hashed)) 
    //en base de donnee
    $query = $db->prepare("INSERT INTO auth_tokens(expires,selector, id_user, token)
                           VALUES(DATE_ADD(NOW(), INTERVAL 14 DAY), :selector, :id_user, :token) ");
    $query->execute(
      [
         'selector'=>$selector,
         'id_user' =>$id_user,
         'token'   =>hash('sha256', $token) 
      ]);
      
     

    // Creer un cookie 'auth' (14 jrs expires) httpOnly => true
     // Contenu: base64_encode(selector).':'.base64_encode(token)

     setcookie('auth', base64_encode($selector).':'.base64_encode($token),
     time()+1209600, null, null, false, true);

     }
 }

 // Auto login
if (!function_exists('auto_login')) {
  
  function auto_login(){
    global $db;

    // Verifier est ce que le cookie auth existe
    if (!empty($_COOKIE['auth'])) {
      
        $slipt = explode(':', $_COOKIE['auth']);
        if (count($slipt)!==2) {
          return false;
        }
      // Recuperer via ce cookie $selector , $token
        list($selector,$token) = $slipt;
        
         // Decoder notre $selector
      // Verifier au niveau de auth_tokens qu'il y a 
      //un enregistrement qui a comme selecteur $selctor

        $query = $db->prepare("SELECT  auth_tokens.token ,auth_tokens.id_user,
                                       users.id, users.pseudo,users.avatar,users.email
                              FROM auth_tokens
                              LEFT JOIN users
                              ON auth_tokens.id = users.id
                              WHERE selector = ? AND expires >= CURDATE() ");
        $query->execute([ base64_decode($selector) ]);
        $data = $query->fetch(PDO::FETCH_OBJ);
        // Si on trouve un enregistrement 
        // comparer les deux tokens
        // Si tou est bon
        // Enrgistrer nos nos infos en session

        if ($data) {
          if (hash_equals($data->token, hash('256', base64_decode($token)))) {

            session_regenerate_id(true);
            $_SESSION['id_user'] =   $data->id;
            $_SESSION['pseudo']  =   $data->pseudo;
            $_SESSION['email']   =   $data->email;
            $_SESSION['avatar']  =   $data->avatar;
            return true;
          }
      }
      

   
             
    // return true

    }
    
    // Dans le cas contraire on return false
      return false;

  }
 }

 // check an user is connected
if (!function_exists('is_logged_in')) {
 	
 	function is_logged_in(){
 			
 		return isset($_SESSION['id_user']) || isset($_SESSION['pseudo']) ;
 				
 	}
 }

 // get avatar url
if (!function_exists('get_avatar_url')) {
 	
 	function get_avatar_url($email,$zise=25){
 	     return 
 	     "http://gravatar.com/avatar/".md5(strtolower(trim(echappe($email))))."?s=".$zise."&d=mm" ;
 	}
 }

 // find users by id
if (!function_exists('find_user_by_id')) {
 	
 	function find_user_by_id($id){
 			
 			global $db;
 			$query = $db->prepare('SELECT name,avatar,pseudo, email,city,country,twitter,
 				github,sexe,avaible_for_hiring,bio FROM users WHERE id=?');
 			$query->execute([$id]); 
            // on stock tous les infos dans $data
 			$data = $query->fetch(PDO::FETCH_OBJ) ;
 			$query->closeCursor();
 			return $data;

 	}
 }

 // find codes by id
if (!function_exists('find_code_by_id')) {
 	
 	function find_code_by_id($id){
 			
 		global $db;
 		$query = $db->prepare("SELECT code FROM codes WHERE id = ?");
        $query->execute([$id]);

        // on stock tous les infos dans $data
 		$data = $query->fetch(PDO::FETCH_OBJ) ;
 		$query->closeCursor();
 		return $data;

 	}
 }
 


// methode permettant de verifier l'existance d'une constante
if (!function_exists('no_empty')) {

	function no_empty($fields= []){


        if (count($fields)!=0) {

        	foreach ($fields as $field) {
				//si un champ est vide , on return false
				if (empty($_POST[$field]) || trim($_POST[$field])=="") {
					 
					 return false;
				}
		    }
		    // si on parcourt tous les champs et aucun champs n'est vide on return true
		    return true;

           // on parcours tous les champs du tableau
	    }
		

	}
       
   
}
 if (!function_exists('is_already_in_use')) {

 	 function is_already_in_use($field,$value,$table){
          global $db;

          $query = $db->prepare("SELECT id FROM $table WHERE $field=?");
          $query->execute([$value]);

          $result=$query->rowCount();
          $query->closeCursor();

          return $result;
 	 }
 	
 }

 if (!function_exists('set_flash')) {
 	
 	function set_flash($message,$type='info'){
 		$_SESSION['notification']['message']=$message;
 		$_SESSION['notification']['type']=$type;
 	}
 }
// Permettant de rediger l'utilsateur vers une autre page en entree
 if (!function_exists('redirection')) {
 	
 	function redirection($page){
 		header('Location:' .$page);
 		exit(); 
 	}
 }
 // Rediriger l'utilisateur vers la page ou il voulez acceder 
 if (!function_exists('redirection_entension')){ 
  
  function redirection_entension($default_url){
     if ($_SESSION['forwarding_url']){
       
        $url = $_SESSION['forwarding_url'];
        $_SESSION['forwarding_url'] = null;

      }
      else{

       $url = $default_url;
      }
      redirection($url);
  }
   
  }

 // garder les infos en session des au'on trouve une erreur
 if (!function_exists('garder_infos_saisis')) {
 	
 	function garder_infos_saisis(){
 		foreach ($_POST as $key => $value) {
 			if (strpos($key, 'password')==false) {
 				$_SESSION['input'][$key]=$value;
 			}
 		}
 	}
 }
 
 // fonction permettant de recuper les infos en session des qu'on trouve une erreur
 if (!function_exists('recupere_infos_saisis')) {
 	
 	function recupere_infos_saisis($key){
 		if (!empty($_SESSION['input'][$key])) {
 			return echappe($_SESSION['input'][$key]);
 		}else{
 			return null;
 		}
 		
 	}
 }
 // fonction permettant de supprimer les infos en session 
 if (!function_exists('supprimer_les_donnees')) {
 	
 	function supprimer_les_donnees(){
 		if (isset($_SESSION['input'])) {
 			 $_SESSION['input'] =[];
 		
 		}
 		
 	}
 }

 // fonction permettant d'echapper les infos que l'utilisateur a saisi 
 if (!function_exists('echappe')) {
 	
 	function echappe($string){
 		// si la chaine est definie
 		if ($string) {
 			return htmlspecialchars($string);
 			//return htmlentities($string,ENT_QUOTES,UTF-8,false);
 		}
 		
 		
 	}
 }
// Gerer l'etat active de nos differentes lien
  if (!function_exists('set_active')) {
 	
 	function set_active($file ,$class="active"){
 		$page = array_pop(explode('/', $_SERVER['SCRIPT_NAME']));
 		if ($page == $file.'.php') {
 			return "active";
 		}else{
 			return " ";
 		}
 	}
 }
// hash password
 if (!function_exists('bcrypt_hash_password')) {
 	
 	function bcrypt_hash_password($value, $option = array()){
 		$cost = isset($option['rounds']) ? $option['rounds']: 10;
 		$hash = password_hash($value, PASSWORD_BCRYPT,array('cost' =>$cost));
        if ($hash==false) {
        	
        	throw new Exception("Bcrypt hashing n'est pas supporte.");
        }
        return $hash;
 		
 	}
 }

 // verifie password
 if (!function_exists('bcrypt_verify_password')) {
 	
 	function bcrypt_verify_password($value,$hashValue){
 
        return password_verify($value,$hashValue);
 	
 	}
 }

?> 