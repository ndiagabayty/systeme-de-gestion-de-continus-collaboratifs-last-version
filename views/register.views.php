
<?php $tilte='Inscription';?>


<?php include("partials/_header.php"); ?>
    
    
    <div class="main-content" style="background-image:url('image/ac.jpg') ; height: 620px ;position: fixed; width: 1500px">
         <h1 class="panel-primary" id="con">logo</h1>
         <div class="container" id="locat">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-primary panel-position">
            <div class="panel-heading">
                <h3 class="panel-title">Devenez deja present membre!</h3>
            </div>
            <div class="panel-body">
          

          <?php
                    include("partials/_error.php");
          ?>

          <form data-parsley-validate  method="post"   >

            <!--  name field  -->
            <div class="form-group">
              <label class="control-label" for="name"><i class="fa fa-edit icons"></i> Nom:</label>
              <input class="form-control" value="<?= recupere_infos_saisis('name') ?>" type="text" name="name" id="name" required="required">
            </div>

             <!-- pseudo field  -->
            <div class="form-group">
              <label class="control-label" for="pseudo">
               <span class="glyphicon glyphicon-user icons"></span> Pseudo:</label>
              <input class="form-control" value="<?= recupere_infos_saisis('pseudo') ?>" type="text" name="pseudo" id="pseudo" required="required" data-parsley-minlength="3">
            </div>

             <!-- email field -->
            <div class="form-group">
              <label class="control-label" for="email">
              <span class="glyphicon glyphicon-envelope icons"></span> Adresse Email:</label>
              <input class="form-control" value="<?= recupere_infos_saisis('email') ?>" type="email" name="email" id="email" required="required" data-parsley-trigger="keypress">
            </div>

              <!-- password field  -->
            <div class="form-group">
              <label class="control-label" for="password"><span class="glyphicon glyphicon-lock icons"></span></i> Mot de Passe:</label>
              <input class="form-control" type="password" name="password" id="password" required="required">
            </div>

             <!-- Fconfirmation field  -->
            <div class="form-group">
              <label class="control-label" for="password_confirm"><span class="glyphicon glyphicon-lock icons"></span></i> Confirmer votre mot de passe:</label>
              <input class="form-control" type="password" name="password_confirm" id="password_confirm" required="required" data-parsley-equalto="#password">
            </div>
            
            <input class="btn btn-primary " type="submit" name="register" value="Inscription">

          </form>

         </div>  

         </div>
      </div>  

    </div>
    </div>

     <?php include("partials/_footer.php"); ?>

 