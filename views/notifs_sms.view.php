<?php $title = "message"; ?>
<?php include('partials/_header.php'); ?>

 <div id="main-content">
 <div class="container">

	 <?php if(count($messages) > 0): ?>
	 	<div class="panel panel-primary">
            <div class="panel-heading">
	            <h3 class="panel-title">Messages reçus</h3>
	        </div>
	            <div class="panel-body">
	                <ul class="list-group">
			            <?php foreach($messages as $message): ?>
				        <li class="list-group-item

				        <?= $message->seen == '0' ? 'not_seen' : '' ?>" >
				        
						 <?php require("partials/messages/{$message->name}.php"); ?>
						 </li>
			 <?php endforeach; ?>
		 </ul>

            </div>
          </div>
	 
          <div id="pagination"><?= $pagination ?></div>
	 <?php else: ?>
	 <p>Aucune message disponible pour l'instant.</p>
	 <?php endif; ?>
 </div>
 </div>
  <!-- SCRIPTS -->
 <script src="assets/js/jquery.min.js"></script>
 <script src="assets/js/main.js"></script>
 <script src="assets/js/bootstrap.min.js"></script>
 <script src="assets/js/jquery.timeago.js"></script>
 <script src="assets/js/jquery.timeago.fr.js"></script>
 <script type="text/javascript">
 $(document).ready(function() {
 $(".timeago").timeago();
 });
 </script>
 </body>
 </html>