
<?php $tilte='Connexion';?>


<?php include("partials/_header.php"); ?>

<div id="main-content">
<div class="container">
<?php if(count($connecteds) > 1 ): ?>	    
<div class="panel panel-primary">
    <div class="panel-heading">
	    <h3 class="panel-title">Amis en ligne  <i class="amis fa fa-circle"></i></h3>
	</div>
	<div class="panel-body" >
	    <ul class="list-group " >
            <?php foreach($connecteds as $connected): ?>
            	<li class="list-group-item " >
                <?php if (current_user_is_freind_with($connected->id_user)):  ?>
	            <?php if ($_GET['id']!=$connected->id_user):  ?>     
				      <a href="profile.php?id=<?=echappe($connected->id) ?>">
				        <img src="<?= $connected->avatar ? $connected->avatar :
				          get_avatar_url($connected->email,100) ?>" width ="30" height="30"
				          alt ="<?=echappe($connected->pseudo) ?>"
				          class= "img-circle">
				      </a>
				       <a href="profile.php?id=<?=echappe($connected->id_user) ?>">
				         <?=echappe($connected->pseudo) ?>   <i class="amis fa fa-circle" ></i>
				       </a> est en ligne
				         <span class="timeago" title="<?= $connected->created_at ?>">
                         <?= $connected->created_at ?></span>
				          
				          <a class="btn btn-primary btn-en" href="new_message.php?id=<?=$connected->id_user?>"><i class="fa fa-envelope " style="color:#4f4;"></i> 
				          Envoyer un message</a>
				  
                <?php endif ?>
			    <?php endif ?> 
			    </li>
             <?php endforeach ?>
	           </ul>

            </div>
          </div>

	  <?php else: ?>
	  	    <div class="col-md-6 col-md-offset-3">
            	    <div class="panel panel-primary">
		                <div class="panel-heading">
		                	<h3 class="panel-title" style="text-align: center;">Aucune amis en ligne pour l'instant</h3>
	                    </div>
	                </div>
	        </div>
	            
	 <?php endif; ?>    
	 
     </div>
    </div>          
    
  <!-- SCRIPTS -->
 <script src="assets/js/jquery.min.js"></script>
 <script src="assets/js/main.js"></script>
 <script src="assets/js/bootstrap.min.js"></script>
 <script src="assets/js/jquery.timeago.js"></script>
 <script src="assets/js/jquery.timeago.fr.js"></script>
 <script type="text/javascript">
 $(document).ready(function() {
 $(".timeago").timeago();
 });
 </script>
 </body>
 </html>




















