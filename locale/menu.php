<?php
    $menu = [
    'accueil' =>['fr' => 'Accueil', 'en' => 'Home'],
    'connexion' =>['fr' => 'Connexion', 'en' => 'Log in'],
    'inscription' =>['fr' => 'Inscription', 'en' => 'Register'],
    'mon_profil' =>['fr' => 'Mon profil', 'en' => 'My Account'],
    'change_password' =>['fr' => 'Changer mon mot de passe', 'en' => 'Change my password'],
    'edit_profil' =>['fr' => 'Editer mon profil', 'en' => 'Edit my Account'],
    'friends' =>['fr' => 'Mes amis', 'en' => 'list frinds'],
    'instantane' =>['fr' => 'Amis en ligne', 'en' => 'list frinds'],
    'share_code' =>['fr' => 'Partager de code', 'en' => 'Share'],
    'deconnexion' =>['fr' => 'Deconnexion', 'en' => 'Logout'],

    ] ;

?>