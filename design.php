<?php $tilte='Changement de mot de passe';?>


<?php include("partials/_header.php"); ?>
    
    
<div class="main-content">
    <div class="container">
        <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
		        <div class="panel-heading">
		            <h3 class="panel-title">Changer de mot de passe</h3>
		            </div>
		            <div class="panel-body">
		                <?php include('partials/_error.php'); ?>

		                <form data-parsley-validate method="post" 
		                	    autocomplete="off">
		                	<div class="form-group">
		                	  	<label for="current_password">Mot de passe actuel<span class="text-danger">*</span></label>
		                	  	<input  type="password" name="current_password" id="current_password" data-parsley-minlength="6"
		                	  			class="form-control"
		                	  			required="required" />
		                	  			
		                	</div>

		                	<div class="form-group">
		                	  	<label for="new_password">Nouveau mot de passe <span class="text-danger">*</span></label>
		                	  	<input  type="password" name="new_password" id="new_password" 
		                	  			class="form-control" data-parsley-minlength="6"
		                	  			required="required" />
		                	  			
		                	</div>

		                	<div class="form-group">
		                	  	<label for="new_password_confirm">Confirmer votre nouveau de passe <span class="text-danger">*</span></label>
		                	  	<input  type="password" name="new_password_confirm" 
		                	  		id="new_password_confirm" data-parsley-equalto="#new_password"
		                	  			class="form-control"
		                	  			required="required" />
		                	  			
		                	</div>

		                	<input type="submit" class="btn btn-primary" name="change_password" value="Valider">


		                </form>

		            </div>
                </div>  
            		
        </div>
            
       </div>
    </div>
</div>
 <?php include('partials/_footer.php'); ?>
