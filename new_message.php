<?php
 session_start();

 require("includes/init.php");
 include('filters/auth_filter.php');

 // le formulaire a ete soumis
    if (isset($_POST['envoyer'])) {

       // si tous les champs ont ete remplies
      if (no_empty(['content']) )
      {
        extract($_POST);
        $id = $_GET['id'];
         $query = $db->prepare('INSERT INTO message(id_content, content,name, id_user)
                          VALUES(:id_content, :content, :name, :id_user)');
         $query ->execute(
          [
           'id_content' => $id,
           'content' => $content,
           'name' => 'friend_request_sent_sms',
           'id_user' => get_session('id_user')
          ]);

          set_flash("Votre message a ete envoyer avec succes!","success");
          redirection('profile.php?id='.get_session('id_user'));
      }else{
          redirection('profile.php?id='.get_session('id_user'));
         }


    }
    elseif(isset($_POST['partager'])){

        redirection('index.php');
    }
?>

 <?php  require("views/new_message.view.php"); 
 ?>