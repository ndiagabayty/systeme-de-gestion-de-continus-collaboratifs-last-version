<?php
 session_start();
 require("includes/init.php");
 require("filters/auth_filter.php");

 if (!empty($_GET['id'])) { 
 	
 	$query = $db->prepare("SELECT user_id FROM microposts 
 		                   WHERE id = :id");
 	$query->execute(
 		[
 		   'id' => $_GET['id']
 		]
 		);
 	$data = $query->fetch(PDO::FETCH_OBJ);
 	$id_user = $data->user_id;
 	// si c'est vraiment cette utilisateur qui a poste ce micropost
 	if ($id_user==get_session('id_user')) {

 		$query = $db->prepare("DELETE FROM microposts WHERE id = :id");
 		$query->execute(
 			[
 			   'id'=>$_GET['id']
 			]
 			);
 		set_flash("Votre publication a ete supprime avec success!");
 	}
 }

 redirection('profile.php?id='.get_session('id_user'));

 ?>