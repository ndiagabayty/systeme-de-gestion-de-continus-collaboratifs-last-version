<?php
session_start();

// sauvegarde de la langue courant
$current_locale = $_SESSION['locale'];
// Supprimer l'entrer en bdb au niveau de auth_tokens 
 require "config/database.php";
 $query = $db->prepare("DELETE FROM auth_tokens WHERE id_user = ? ");
 $query->execute([$_SESSION['id_user']]);

 // On supprime les entree de l'utilisateur presentement deconnectee dans la table connected

 $query = $db->prepare("DELETE FROM connected WHERE id_user = ? ");
 $query->execute([$_SESSION['id_user']]); 
 
 // Reinitialisation de la session
 $session_keys_white_list = ['locale'] ;
 $new_session = array_intersect_key($_SESSION, array_flip($session_keys_white_list));
 $_SESSION = $new_session ;
 
// Suppression des cookies et detruit la session
setcookie('auth' , ''  , time()-3600);



// Restauration de la langue choisi par l'utlisateur et redirection

 header('Location: login.php');



?>