<?php

	if (!isset($_SESSION['id_user']) && !isset($_SESSION['pseudo']))  {
		$_SESSION['forwarding_url'] = $_SERVER['REQUEST_URI'];
		$_SESSION['notification']['message']='Vous devez etre connecte pour acceder a cette page.';  
 		$_SESSION['notification']['type']='danger';
		
		header('Location: login.php');
		exit();
	}
?>