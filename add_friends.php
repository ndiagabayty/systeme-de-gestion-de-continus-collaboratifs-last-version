<?php
 session_start();
 require("includes/init.php");
 require("filters/auth_filter.php");

 if (!empty($_GET['id'])  &&  $_GET['id']!==get_session('id_user')) {
 	 
 	 if (!if_a_friend_request_already_has_been_sent(get_session('id_user'),$_GET['id'])) {
 	 	$id = $_GET['id'];
 	 	// traitement
	 	$query = $db->prepare("INSERT INTO friends_relationships(id1_user,id2_user)
	 	                       VALUES (:id1_user, :id2_user)");
	 	$query->execute(
	 		[ 
	 		    'id1_user' =>get_session('id_user'),
	 		    'id2_user' =>$id 
	 		]);

        // Sauvegarde de la notification
		$query = $db->prepare('INSERT INTO notifications(id_subject, name, id_user)
			                   VALUES(:id_subject, :name, :id_user)');
	    $query->execute(
	    	[
			 'id_subject' => $id,
			 'name' => 'friend_request_sent',
			 'id_user' => get_session('id_user')
		    ]);

	 	set_flash("Votre demande d'amitié a ete envoyée avec succès!");
	 	redirection('profile.php?id='.$id);

	 }else{

	 	set_flash("Cet utilisateur vous a déja envoyé une demande d'amitié.");
	 	redirection('profile.php?id='.$_GET['id']);
	 }
 	
 }else{
 	redirection('profile.php?id='.get_session('id_user'));
 }

?>