
<?php

   // Pour pouvoir utliser $_SESSION  77 109 05 93
   session_start();
   require("includes/init.php");
   require("filters/auth_filter.php");
   
   
   if(isset($_POST['publier'])){

   	  if (!empty($_POST['content'])) {
   	  	
   	  	 extract($_POST);
          if (mb_strlen($content)<3 || mb_strlen($content)>140) {
             set_flash("Contenu invalide ( Minimun 3 caracteres | Maximum 140 caracteres) !","danger");
          }else{

             create_micropost_for_the_current_user($content);
          }

   	  	
   	  }
   }
   redirection('profile.php?id='.get_session('id_user'));

   ?>