
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="reseau social pour developpeur">
    <meta name="author" content="NDIAGA GUEYE">  
    <link rel="icon" type="image/png" href="image/favicon.jpg" />
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" >
    <link rel="stylesheet" type="text/css" href="assets/css/main.css"> 
    <link rel="stylesheet" type="text/css" href="assets/google-code-prettify/prettify.css">
    <link rel="stylesheet" type="text/css" href="librairies/uploadify/uploadify.css"> 
    <link rel="stylesheet" type="text/css" href="librairies/alertifyjs/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="librairies/alertifyjs/css/themes/bootstrap.min.css"> 
    <link rel="stylesheet" href="assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="librairies/sweetalert/sweetalert.css">


      <!--  supplementaire  -->

      
      
    <title>
          <?= isset($title)? $title.'-'.WEBSITE_NAME:
            WEBSITE_NAME.'simple rapide efficase'; 
            
           ?>
    </title>
      
  </head>

  <body>

  <?php include("partials/_nav.php"); ?>
  <?php include("partials/_flash.php"); ?>